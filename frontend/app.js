//Selectors
const taskInput = document.querySelector(".task-input");
const inputButton = document.querySelector(".task-input-button");
const tasks = document.querySelector(".tasks");
const filterOption = document.querySelector(".filter-tasks");

//Event Listeners
document.addEventListener("DOMContentLoaded", getTasksFromDb);
inputButton.addEventListener("click", addTask);
tasks.addEventListener("click", deleteCheck);
filterOption.addEventListener("click", filterTasks);



//Functions
const getData = () => {
    return axios.get('/getTodo')
    .then(function (response) {
        console.log(response);
        return response.data
    })
    .catch(function (error) {
        console.log(error);
    })
    
};

const postData = (todo) => {
    axios.post('/postTodo', {
        todo: todo
      })
      .then(function (response) {
        console.log(response);
      })
      .catch(function (error) {
        console.log(error);
      });
}

const deleteData = (todo) => {
    //debugger
    axios.delete('/deleteTodo/', {
        todo: todo
      })
      .then(function (response) {
        console.log(response);
      })
      .catch(function (error) {
        console.log(error);
      });
}

async function getTasksFromDb(){
    let data = await getData()
   // debugger;
    let parsedData = data
    //let parsedData = JSON.parse(data);
    console.log(parsedData)

    parsedData.forEach(function(task){
        const taskDiv = document.createElement("div");
        taskDiv.classList.add("task-div");
    
        const newTask = document.createElement("li");
        newTask.classList.add("new-task");
        newTask.innerText = task.todo;
        taskDiv.appendChild(newTask); 
        
        const checkButton = document.createElement("button");
        checkButton.innerHTML = "<i class= 'fas fa-check-square'></i>";
        checkButton.classList.add("check-button");
        taskDiv.appendChild(checkButton);
    
        const trashButton = document.createElement("button");
        trashButton.innerHTML = "<i class ='fas fa-trash-alt'></i>";
        trashButton.classList.add("trash-button");
        taskDiv.appendChild(trashButton);
    
        tasks.appendChild(taskDiv);
        taskInput.value = "";
    })
}

function addTask(event) {
    event.preventDefault();

    //create taskDiv - new task container that contains newtask text, check button, delete button
    const taskDiv = document.createElement("div");
    taskDiv.classList.add("task-div");

    //create new task - text of new task
    const newTask = document.createElement("li");
    newTask.classList.add("new-task");
    newTask.innerText = taskInput.value;
    taskDiv.appendChild(newTask);

    //saveLocalTasks(taskInput.value)
    postData(newTask.innerText)

    //create check button
    const checkButton = document.createElement("button");
    checkButton.innerHTML = "<i class= 'fas fa-check-square'></i>";
    checkButton.classList.add("check-button");
    taskDiv.appendChild(checkButton);

    //create delete button
    const trashButton = document.createElement("button");
    trashButton.innerHTML = "<i class ='fas fa-trash-alt'></i>";
    trashButton.classList.add("trash-button");
    taskDiv.appendChild(trashButton);

    tasks.appendChild(taskDiv);
    taskInput.value = "";
}




//if button pressed is check button - mark as checked, if button pressed is  trash button - delete item
function deleteCheck(e) {
    const button = e.target;
    console.log(button.parentElement);
    if(button.classList[0] === "trash-button") {
        const task = button.parentElement;
        task.classList.add("fall");
        let target = null
        for (let i = 0; i<task.childNodes.length; i++){
            if (task.childNodes[i].className == "new-task"){
                target = task.childNodes[i];
                break;
            }
        }
        
        deleteData(target.innerText)
        debugger;
        
        task.addEventListener("transitionend", function() {
            task.remove();
        });
    }
    if(button.classList[0] === "check-button") {
        const taskDiv = button.parentElement;
        taskDiv.classList.toggle("completed");
    }
}

function filterTasks(e) {
    const allTasks = tasks.childNodes;
    allTasks.forEach(function(task) {
        switch(e.target.value) {
            case "all":
                task.style.display = "flex";
                break;
            case "completed":
                if(task.classList.contains("completed")){
                    task.style.display = "flex";
                }else{
                    task.style.display = "none";
                }
                break;
            case "uncompleted":
                if(!task.classList.contains("completed")){
                    task.style.display = "flex";
                }else{
                    task.style.display = "none";
                }
                break;
        }
    });
}

/*
//old ways of saving tasks localy without database
function saveLocalTasks(task){
    let tasks;
    if( localStorage.getItem("tasks") === null) {
        tasks = [];
    } else {
        tasks = JSON.parse(localStorage.getItem("tasks"));
    }
    tasks.push(task);
    localStorage.setItem("tasks", JSON.stringify(tasks));
}
*/





/*
function getTasks(){
    /*
    if( localStorage.getItem("tasks") === null){
        allTasks = [];
    } else {
        allTasks = JSON.parse(localStorage.getItem("tasks"))
    }
    
   debugger
    allTasks.forEach(function(task){
        event.preventDefault();
        const taskDiv = document.createElement("div");
        taskDiv.classList.add("task-div");
    
        const newTask = document.createElement("li");
        newTask.classList.add("new-task");
        newTask.innerText = task;
        taskDiv.appendChild(newTask);
    
        const checkButton = document.createElement("button");
        checkButton.innerHTML = "<i class= 'fas fa-check-square'></i>";
        checkButton.classList.add("check-button");
        taskDiv.appendChild(checkButton);
    
        const trashButton = document.createElement("button");
        trashButton.innerHTML = "<i class ='fas fa-trash-alt'></i>";
        trashButton.classList.add("trash-button");
        taskDiv.appendChild(trashButton);
    
        tasks.appendChild(taskDiv);

    });
}

function removeLocalTasks(task){
    if( localStorage.getItem("tasks") === null) {
        allTasks = [];
    } else {
        allTasks = JSON.parse(localStorage.getItem("tasks"));
    }
    const taskIndex = task.children[0].innerText;
    allTasks.splice(allTasks.indexOf(taskIndex),1);
    localStorage.setItem("tasks", JSON.stringify(allTasks));
}

*/


